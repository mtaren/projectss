#include "scheduler_abs.h"
#include "simulator.h"
#include "config.hpp"
#include "thread.h"
#include "performance_model.h"
#include "core_manager.h"
#include "misc/tags.h"
#include "rng.h"

//
#include "thread_stats_manager.h"


SchedulerABS::SchedulerABS(ThreadManager *thread_manager)
   : SchedulerPinnedBase(thread_manager, SubsecondTime::NS(Sim()->getCfg()->getInt("scheduler/abs/quantum")))
   , m_debug_output(Sim()->getCfg()->getBool("scheduler/abs/debug"))
   , m_last_reshuffle(SubsecondTime::Zero())
   , m_rng(rng_seed(42))
{
   // Figure out big and small cores, and create affinity masks for the set of big cores and the set of small cores, respectively
   m_num_big_cores = 0;
   CPU_ZERO(&m_mask_big);
   CPU_ZERO(&m_mask_small);

   for (core_id_t coreId = 0; coreId < (core_id_t) Sim()->getConfig()->getApplicationCores(); coreId++)
   {
      bool isBig = Sim()->getTagsManager()->hasTag("core", coreId, "big");
      if (isBig)
      {
         ++m_num_big_cores;
         CPU_SET(coreId, &m_mask_big);
      }
      else
      {
         CPU_SET(coreId, &m_mask_small);
      }
   }
}

void SchedulerABS::threadSetInitialAffinity(thread_id_t thread_id )
{
   // All threads start out on the small core(s)
  // Sim()->getThreadStatsManager()->threadStart(thread_id, SubsecondTime::Zero());
   moveToSmall(thread_id);
}

void SchedulerABS::threadStall(thread_id_t thread_id, ThreadManager::stall_type_t reason, SubsecondTime time)
{
   // When a thread on the big core stalls, promote another thread to the big core(s)
  
 //  if (m_debug_output)
      std::cout << "[SchedulerABS] thread " << thread_id << " stalled" << std::endl;

   if (m_thread_isbig[thread_id])
   {
      // Pick a new thread to run on the big core(s)
      pickBigThread_(time);
      // Move this thread to the small core(s)
      moveToSmall(thread_id);
   }

   // Call threadStall() in parent class
   SchedulerPinnedBase::threadStall(thread_id, reason, time);

   if (m_debug_output)
      printState();
}

void SchedulerABS::threadExit(thread_id_t thread_id, SubsecondTime time)
{
   // When a thread on the big core ends, promote another thread to the big core(s)

   //if (m_debug_output)
      std::cout << "[SchedulerABS] thread " << thread_id << " ended" << std::endl;

   if (m_thread_isbig[thread_id])
   {
      // Pick a new thread to run on the big core(s)
      pickBigThread_(time);
   }

   // Call threadExit() in parent class
   SchedulerPinnedBase::threadExit(thread_id, time);

   if (m_debug_output)
      printState();
}

void SchedulerABS::periodic(SubsecondTime time)
{
   bool print_state = false;

   if (time > m_last_reshuffle + m_quantum)
   {
      // First move all threads back to the small cores
      for(thread_id_t thread_id = 0; thread_id < (thread_id_t)Sim()->getThreadManager()->getNumThreads(); ++thread_id)
      {
         if (m_thread_isbig[thread_id]) //thread is on big core
            moveToSmall(thread_id);
      }
      // Now promote as many threads to the big core pool as there are big cores
      for(UInt64 i = 0; i < std::min(m_num_big_cores, Sim()->getThreadManager()->getNumThreads()); ++i)
      {
	 if (m_debug_output){
	    std::cout << "[SchedulerABS] periodic " << " iteration "<< i <<std::endl;}
         pickBigThread_(time);
      }

      m_last_reshuffle = time;
      print_state = true;
   }

   // Call periodic() in parent class
   SchedulerPinnedBase::periodic(time);

   if (print_state && m_debug_output)
         printState();
}

void SchedulerABS::moveToSmall(thread_id_t thread_id)
{
   threadSetAffinity(INVALID_THREAD_ID, thread_id, sizeof(m_mask_small), &m_mask_small);
   m_thread_isbig[thread_id] = false;
}

void SchedulerABS::moveToBig(thread_id_t thread_id)
{
   threadSetAffinity(INVALID_THREAD_ID, thread_id, sizeof(m_mask_big), &m_mask_big);
   m_thread_isbig[thread_id] = true;
}

void SchedulerABS::pickBigThread(){}
void SchedulerABS::pickBigThread_(SubsecondTime time)
{
   // First build a list of all eligible cores
   std::vector<thread_id_t> eligible;

   for(thread_id_t thread_id = 0; thread_id < (thread_id_t)Sim()->getThreadManager()->getNumThreads(); ++thread_id)
   {
//      if (m_thread_isbig[thread_id] == false && m_thread_info[thread_id].isRunning())
      if (m_thread_info[thread_id].isStarted() && m_thread_isbig[thread_id] == false && m_thread_info[thread_id].isRunning())
      {
 	     if (m_debug_output)
         	std::cout << "[SchedulerABS] eligible thread " << thread_id <<" @ core "<< m_thread_info[thread_id].getCoreRunning()<< std::endl;
         eligible.push_back(thread_id);
      }
   }

   if (m_debug_output)
      std::cout << "[SchedulerABS] eligible list is done size?" << eligible.size() <<" /  " << Sim()->getThreadManager()->getNumThreads()<<std::endl;
   if (eligible.size() > 0)
   {
      // Choose thread whose # of instructions is smallest
	  bool isFirst = true;
	  thread_id_t thread_id  = 0; //= eligible[rng_next(m_rng) % eligible.size()];
	  uint64_t inst_smallest;
	  for(int thread_id_ = 0; thread_id_ < (int)eligible.size(); ++thread_id_)
	  {
	  	int inst = 0;
//		if(m_thread_info[eligible[thread_id_]].isStarted())	
				inst = Sim()->getThreadStatsManager()->getThreadStatistic(eligible[thread_id_], ThreadStatsManager::INSTRUCTIONS);
		if (m_debug_output){
		  std::cout << "[SchedulerABS] thread " << eligible[thread_id_] <<  " # of instructions " << inst << std::endl;
	  	}

		if(!isFirst)
		{	
		  if(inst_smallest > inst && inst !=-1)
		  {
		    if (m_debug_output)
		      std::cout << "[SchedulerABS] thread " << eligible[thread_id_] << " updated smallest" << std::endl;
		    thread_id = eligible[thread_id_];
		    inst_smallest = inst;
		  }
		}else
		{
		  if (m_debug_output)
		    {std::cout << "[SchedulerABS] thread " << eligible[thread_id_] << " first" << std::endl;}
		  thread_id = eligible[thread_id_];
		  inst_smallest = inst;
		  isFirst = false;
		}
	  }
	  moveToBig(thread_id);

      if (m_debug_output)
	{std::cout << "[SchedulerABS] thread " << thread_id << " promoted to big core" << std::endl;}
   }
}
