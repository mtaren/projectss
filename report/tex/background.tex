\section{Background: Unit Gate Model Analysis}
\label{sec:background}
In this section, we introduce unit gate model analysis
used in this work and summarize
the analysis result of Mitchell's approximation
derived in \cite{mike13}.
\subsection*{Unit Gate Model}
Unit gate model applied in this work characterizes each design element 
to consider two metrics, \textit{cost} and \textit{delay}.
Cost $C$ shows an abstract value that can be assumed to be proportional
to complexity (area) and power consumption, while delay $T$ shows
an estimation of actual delay over circuits.
Although two metrics are rough estimations,
they are useful enough to represent the expected real cost and performance through high-level analysis.
Basic assumptions of the unit gate model are:
\setlist[1]{itemsep=-5pt}
\begin{itemize}
		\item Basic 2-input gates (AND, OR) [$C=1, T=1$]
		\item 2-input XOR gates, MUXes and HA cell [$C=2, T=2$]
		\item $m$-input gates [a tree of 2-input gates]
		\item Inversion and  buffering [free]
\end{itemize}

\subsection*{Cost of Mitchell's Approximation}
We also briefly introduce the cost of Mitchell's approximation, 
based on unit gate model described above,
which was originally shown in \cite{mike13}.
\begin{enumerate}
		\item[\textbf{a)}] For a conservative analysis model, a Kogge-Stone style prefix tree
		can be considered. Note that we generally do not want to minimize
		the delay of logarithmic approximation, which is likely to be on the critical path.
		Prefix tree design leads to minimum-depth though it results in maximal complexity as well.
		The cost and delay of LOD are given as below:
		\begin{eqnarray}
				C_{LOD}(n)&=&C_{AND}\cdot \sum_{i=0}^{\log_2{(n)}-1}\frac{n}{2}\nonumber\\		
								   &=&C_{AND}\cdot\frac{n}{2}\cdot \log_2{(n)}\\
				T_{LOD}(n)&=&T_{AND}\cdot\log_2{(n)} 
		\end{eqnarray}
	
\item[\textbf{b)}] Encoder of one-hot output of LOD can be implemented with $\log_2{n}$ $\frac{n}{2}$-input
		OR gates as shown in Figure \ref{fig:enc_1}, and each $\frac{n}{2}$-input OR gate consists of $(\frac{n}{2}-1)$ 2-input
		OR gates because it has tree structure as mentioned above unit gate model.
		Thus, the cost and delay of encoder are given as below:
		\begin{eqnarray}
				C_{ENC}(n)&=&C_{OR}\cdot \log_2{(n)}\cdot\left(\frac{n}{2}-1\right)\\		
				T_{ENC}(n)&=&T_{OR}\cdot (\log_2{(n)} -1)
		\end{eqnarray}
\item[\textbf{c)}] Logarithm Shifter is $(n-1)$-input parallel prefix tree. Note that only $(n-1)$
		bits are considered because the most significant bit can be naturally ignored
		when approximating \lgf{}. 
		The complextity and performance of shifter is determined
		by its width $n$ and its depth $d$, which is equivalent to the width of shift amount.
		Parallel prefix tree for shifters consists of two types of nodes as shown in Figure \ref{fig:shifter}: 
		1) 2-input multiplexer controlled by the encoded shift amount is denoted by black node.
		2) AND gate that represents increasing zeros in the least significant bits
		of shifted output is denoted by gray node.
		At each level of shifter consists of $(n-1)$ bits, and the number of gray nodes
		increases exponentially with depth $d$ as $G_{SHIFT}(d) = 2^d-1$.
		Thus, the cost of delay of encoder are given as below:
		\begin{eqnarray}
		C_{SHIFT}(n,d)&=&C_{MUX}\cdot \left(d(n-1) - G_{SHIFT}(d)\right) + C_{AND}\cdot G_{SHIFT}(d)\nonumber\\
												   &=&C_{MUX}\cdot\left(d(n-1)-2^d+1\right) + C_{AND}\cdot(2^d-1)\\
				T_{SHIFT}(n,d)&=&T_{MUX}\cdot \log_2{(n)} 
		\end{eqnarray}
\end{enumerate}
The critical path of Mitchell's logarithmic conversion goes through
LOD, Encoder and Shifter in series, whose cost can be additive.
In other words, the total cost and delay of Mitchell Approximaiton can be expressed
as the sum of those from above three cost analyses:
		\begin{eqnarray}
				C_{LOG}(n,d)&=&C_{LOD}(n) + C_{ENC}(n) + C_{SHIFT}(n,d)\\
		  T_{LOG}(n,d)&=&T_{LOD}(n) + T_{ENC}(n) + T_{SHIFT}(n,d) 
		\end{eqnarray}
\begin{figure}[h!]
\centering
   \subfloat[Logarithmic Shifter]{\includegraphics[width=2.4in]{fig/logcon}}
	\subfloat[Anti-logarithmic shifter]{\includegraphics[width=4in]{fig/anti}}
\caption{Two shifters for approximation logarithmic and anti-logarithmic conversions($n=16$)} 
\label{fig:shifter}
\end{figure}
