%%
%% Proposed mechanism - SLP
%%

%===================================================================================
\section{Analysis of Ineffective Compaction}
\label{sec:branch_analysis}

\subsection{Limitation of Previous Compactions}
%\vspace{-0.15in}
\label{sec:limits_of_compaction}

While CAPRI is useful in alleviating the synchronization overhead of compaction, thus avoiding performance loss,
it does not provide practical benefits in terms of resource utilization and performance.
\fig{fig:tbc_benefits} illustrates the average SIMD lane utilization (\simdutil) of benchmarks with and without compaction activated. 
In order to maximally exploit compactable branches, this paper assumes TBC as the baseline compaction mechanism (unless noted otherwise) as CAPRI's misprediction leads
to sub-optimal utilization of compactable branches.
Overall, only 9 out of the 18 divergent benchmarks were shown to achieve improvements in \simdutil, but with less than $2\%$ increase for
5 of them (BITONIC, DXTC, AOSSORT, EIGENVL, SOBELFLT). Interestingly, benchmarks with lower \simdutil~do not necessarily obtain benefits through compaction; 
LPS and BACKP, for instance, have zero increase in \simdutil~while SORTNW and SOBFLT have many compactable branches. At the same time, 
higher \simdutil~benchmarks do not necessarily benefit less than benchmarks with lower \simdutil; BITONIC has a much lower
\simdutil~compared to SORTNW without compaction but achieves smaller increase in \simdutil.
Such result is contradictory to the motivation behind compaction -- assuming active threads are
evenly distributed across the SIMD lanes, probability of successful compaction should be higher for more divergent benchmarks as chances of threads in 
different warps filling in the vacant SIMD lanes is more likely under such assumption. 

\oldtext{
REDUCT, LPS, and BACKP, for instance, have a lower 
\simdutil~than SORTNW and SOBFLT without compaction,
meaning the average number of vacant SIMD lanes is larger for the former three benchmarks. Nonetheless, all three benchmarks achieve zero benefits 
through compaction while the latter two do so. 
}

Accordingly, while a benchmark's absolute \simdutil~is certainly correlated with the compactability 
of its diverging paths (exemplified by BFS and MUM's $73\%$ and $50\%$ increase in \simdutil~respectively), it is not the sole factor 
that determines it. Rather, what's more critical is \emph{how} the divergence is manifested among warps because
only threads that have diverged to the same path \emph{and} do not share a common home SIMD lane can be compacted together (\fig{fig:wcu_example}).
Previous compaction-mechanisms~\cite{fung:2011:tbc,narasiman:2011:lwm,rhu:2012:capri} are therefore only effective on
divergent branches that do not cause active threads to be fully aligned at common SIMD lanes (which we refer to as \emph{aligned divergence} in the rest of this paper).
And as we explore in \sect{sec:key_observations}, significant number of branches do not exhibit such characteristic which limits the applicability
of compaction. The leftmost SIMD lane in \fig{fig:wcu_example}, for instance, is fully
aligned by the three active threads (\NWtbc~and \NWpdom~are therefore both 3), so compaction provides no benefits. An early work of Fung et al.~\cite{fung:2007:dwf}, which was superseded by 
TBC~\cite{fung:2011:tbc}, also discussed how BITONIC can struggle in dynamically reforming smaller number of warps due to the default mapping of home
SIMD lanes to threads. As an optimization to BITONIC, the authors alternately swap the position of even/odd home SIMD lanes for every other warp 
such that SIMD lanes are better occupied. The effectiveness of their technique (\emph{XOR\_odd\_1} in \fig{fig:slp}), however, 
is limited in many cases as we discuss in \sect{sec:analysis}. But more importantly, it lacks the in-depth analysis on when aligned divergence occurs 
and why the alternatives we present can effectively tackle it, which is the main focus of this study.
In the following subsection, we analyze the cause of aligned divergence in detail.

\begin{figure}[t]
\centering
\includegraphics[width=0.41\textwidth]{./fig/programmatic_control_flow}
\caption{Active warp status upon a programmatic value dependent control flow. W$_n$ designates a warp with its warp-ID as \emph{n}.}
\label{fig:programmatic_control_flow}
\end{figure}
\oldtext{
\caption{Active warp status upon a programmatic value dependent control flow. Warps in (a) are typically generated when a 
branch masks out i) all but threads with its thread-ID's index value set, ii) the later half of threads in warp, iii) threads with odd thread-ID, 
and iv) gradually more as warp-ID increase). Warps in (b) are generated when i) warp-ID solely decides the control flow,
ii) only threads with its thread-ID's index value set are masked out, iii) only a few threads at the first warp are active, and 
iv) only a few threads at the last warp are masked out. W$_n$ designates a warp with its warp-ID as \emph{n}.}
}
%===================================================================================
\subsection{Aligned Divergence}
%\vspace{-0.15in}
\label{sec:cause_of_aligned_divergence}

What fundamentally decides the control flow of each thread is how the branching predicate is evaluated. Aligned
divergence is a phenomenon where threads with a common home SIMD lane have their predicate condition resolved the same, 
causing active threads to be concentrated on certain lanes (\fig{fig:programmatic_control_flow}). We observe that such alignment is difficult 
to be exhibited when the predicate depends on \emph{data arrays} (allocated at local/global/shared/constant/texture memory
in CUDA), as different threads most likely reference different values thereby having the predicate condition resolved differently (\fig{fig:example_kernels}(a)). 
The likelihood of aligned divergence is therefore low for such data dependent branch (D-branch), and accordingly, these branches compact relatively well.
The divergence behavior of a predicate condition that does not depend on a data array value (which we refer to as a programmatic value), on the other hand, 
is substantially different compared to D-branches. A \emph{programmatic} value is one that is viewed as a same value (thus is virtually a \emph{constant}) across the threads.
The indices for the CTA-ID (\texttt{blockIdx}), width/height of a CTA (\texttt{blockDim}), and \emph{scalar} input parameters of a CUDA kernel (i.e. \emph{imageW} in \fig{fig:example_kernels}(d)), 
for instance, are all seen as constant values to all the threads within a CTA and are therefore programmatic values. In addition, 
the indexing value of a thread-ID (i.e. \texttt{threadIdx.x, threadIdx.y}) is virtually a constant to the threads sharing the same index value (\fig{fig:example_kernels}(c)),
so it is also programmatic. Compared to D-branches, branches depending on programmatic values (P-branches) are prone to experience aligned divergence because 
the (programmatic) values being used for resolving the predicate is likely the same among threads (i.e. threads within the
same warp or ones sharing a common home SIMD lane).
Although P-branches that cause only a \emph{partial} alignment at SIMD lanes (\fig{fig:programmatic_control_flow}(c)) can be compacted as-is, we observe that
such case is relatively rare compared to P-branches causing \emph{full} alignment, and thus preventing compaction (\fig{fig:programmatic_control_flow}(a,b)).

It is worth mentioning that a branch whose predicate condition depends on \emph{both} programmatic and data value (\fig{fig:example_kernels}(b)) is virtually a 
D-branch; this is because the data each thread is referencing will likely cause the predicate to be evaluated differently, regardless of the programmatic value.
In \sect{sec:key_observations} and \sect{sec:analysis}, we categorize divergent branches into the P-/D-branches and quantitatively verify the above.

\begin{figure}[t]
\centering
\subfloat[Threads that loaded a data array value (\emph{g\_graph\_visited}) as zero only execute the true path. The branch in line 7 is therefore data-dependent.]
%\subfloat[ ]
{
\label{fig:input_dependent_branch}
\includegraphics[width=0.49\textwidth]{./fig/input_dependent_branch}
}
\hfill

\subfloat[The intermediate variable (\emph{ddd}), which is tainted by a programmatic value, is combined with
values from data arrays (\emph{s\_key, s\_val}) to calculate the predicate. As the data value referenced by each threads
is likely dissimilar, branch in line 8 is data-dependent.]
{
\label{fig:both_branch_biased_input}
\includegraphics[width=0.49\textwidth]{./fig/both_branch_biased_input}
}
\hfill

\subfloat[The index value of a thread's thread-ID (\emph{tid}) solely determines whether the true path (\emph{if}) or the false path (\emph{else}) is taken. Hence, the
branch in line 6 is programmatic.]
{
\label{fig:programmatic_branch}
\includegraphics[width=0.49\textwidth]{./fig/programmatic_branch}
}
\hfill

\subfloat[The intermediate variable (\emph{ix, iy}), which is tainted by programmatic values, is combined with
another programmatic value (\emph{imageW, imageH}) from a scalar input parameter of the kernel. The branch in line 6
is therefore programmatic.]
{
\label{fig:both_branch_biased_programmatic}
\includegraphics[width=0.49\textwidth]{./fig/programmatic_and_input_branch}
}
\hfill
\caption{Example kernel codes containing P-branches and D-branches.} 
\label{fig:example_kernels}
\end{figure}

\begin{figure*}[t]
\centering
\subfloat[Breakdown of all dynamically executed conditional branches into i) non-divergent branches (ND), ii) divergent branches with \emph{no} potential compactability (D/NC), 
and iii)  divergent branches \emph{with} potential compactability (D/C). We define a divergent branch as having potential compactability if either the true or false path
is compactable using \TBCideal~(true/false path's CTA-wide mask is used to determine their \NWideal, \NWpdom, and its potential compactability).]
{
\label{fig:divergent_br_among_all}
\includegraphics[width=0.99\textwidth]{./fig/divergent_br_among_all}
}
\hfill
\subfloat[Breakdown of divergent branches (D/NC and D/C in (a)) into P-branches and D-branches.]
{
\label{fig:branch_categorization}
\includegraphics[width=0.99\textwidth]{./fig/branch_categorization}
}
\hfill
\subfloat[Compaction rate of P-/D-branches. Among all the paths that diverged from a branch, the fraction
of paths that are compactable using \TBCideal~(or TBC) is defined as the compaction rate of \TBCideal~(or TBC).]
{
\label{fig:ideal_tbc_compactability}
\includegraphics[width=0.99\textwidth]{./fig/ideal_tbc_compactability}
}
\hfill
\caption{Analysis of the behavior of control divergence and compactability (using GPUOcelot).} 
\label{fig:compaction_analysis}
\end{figure*}

\subsection{Key Observations and Analysis}
\label{sec:key_observations}

Although the divergent threads in \fig{fig:programmatic_control_flow}(a) are not compactable as-is, the \emph{potential} compactability of
these control flows are significant as discussed below. Assuming an \emph{ideal} block compaction mechanism that allows threads to freely switch their 
executing SIMD lanes when compacted (which we refer to as \TBCideal~in the rest of this paper), the \emph{number of compacted warps using
\TBCideal}~(\NWideal) is derived as follows:

\begin{equation}
\label{eq:nwc}
%\mathit{Nwarps_{CompactedIdeal}} = \frac{\sum_{i=1}^{N} NumPath_{i}}{N} 
\mathit{NW_{ideal}} = \lceil \frac{NumActive_{CTA}}{SIMD_{width}} \rceil
\end{equation}

Here, \NumActiveCTA~refers to the \emph{total number of threads active at the diverging path across the CTA} and \SIMDwidth~designates
the \emph{width of the SIMD pipeline}. As the divided value is equal to the \emph{average number of active threads 
that are executing in each SIMD lane}, ceiling this value becomes equivalent to \NWideal.
Using \NWideal, we can determine the potential and actual compactability of diverging paths based on the following. 
When \NWideal~and \NWtbc~(\sect{sec:compaction}) are both smaller than the number of warps without compaction (\NWpdom), this path not only
has potential for compaction but can also be compacted as-is (\fig{fig:programmatic_control_flow}(c)). If, on the other hand, \NWpdom~and \NWtbc~are the same and \NWideal~is 
smaller than \NWpdom, this path is potentially compactable assuming home SIMD lanes can be switched but not as-is (\fig{fig:programmatic_control_flow}(a)).
If the value of \NWideal~and \NWpdom~are the same, the subject path has neither potential nor actual compactability (\fig{fig:programmatic_control_flow}(b)).

Based on the the discussions above, we provide an overall behavioral analysis of branch divergence and its compactability in \fig{fig:compaction_analysis}.
First, \fig{fig:compaction_analysis}(a) shows a breakdown of all dynamically executed branches based on their divergence and potential 
compactability. The divergent branches have been further categorized as P-/D-branches based on a taint analysis (detailed in \sect{sec:sim_method}) 
of the predicates of branch instructions using GPUOcelot~\cite{gpuocelot} (\fig{fig:compaction_analysis}(b)). In addition, we evaluate \TBCideal~and TBC's 
\emph{compaction rate} across the divergent P-/D-branches, as described in \fig{fig:compaction_analysis}(c), in order to compare how much of the
compaction opportunities are actually utilized (TBC) compared to the theoretical upper bound (\TBCideal).

Overall, benchmarks with a nonzero TBC compaction rate in either P-/D-branches (\fig{fig:compaction_analysis}(c)) 
are able to achieve improvements in \simdutil. In fact, all 9 benchmarks containing any D-branches always achieve a nonzero 
TBC compaction rate for this branch type, meaning previous compaction mechanisms work relatively well for D-branches.
Note, however, that among the 15 benchmarks containing P-branches, 13 of them are not able to compact any one of them, 
represented by their zero P-branch TBC compaction rate in \fig{fig:compaction_analysis}(c). Except for the 5 benchmarks
that have no chance of compaction in the first place (zero value for \emph{Ideal} in \fig{fig:compaction_analysis}(c)), substantial opportunity for
improving \simdutil~is wasted, -- for benchmarks containing high fraction of P-branches and high \TBCideal~compaction
rate (BITONIC, REDUCT, LPS, BACKP, AOSSORT, FDTD3D, 3DFD, and QSRDM). This work seeks to tackle 
such P-branches to retrieve compaction opportunities using \emph{SIMD lane permutation}.
In the following section, we detail our proposed SLP mechanism.













